module.exports = {
  env: {
    browser: true,
    es2021: true,
    node: true,
  },
  extends: [
    'airbnb-base',
    'plugin:unicorn/recommended',
  ],

  "plugins": [
    // "sort-keys-fix"
  ],

  parserOptions: {
    ecmaVersion: 'latest',
    sourceType: 'module',
  },
  rules: {
    // "sort-keys": ["error", "asc", {"caseSensitive": true, "natural": false, "minKeys": 2}]
    // "sort-keys-fix/sort-keys-fix": "warn"
  },
};
