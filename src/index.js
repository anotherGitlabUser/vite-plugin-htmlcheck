import { setupServer } from './server.js';

import {
  store,
  injectScriptToClient,
} from './utils/config.js'; // eslint-disable-line import/extensions

import {
  mergeConfig,
} from './utils/config.js'

const htmlCheck = (userConfig = {}) => {
  const config = mergeConfig({ userConfig });

  let viteConfig;

  store.config = config;
  return {
    name: 'vite-plugin-htmlCheck',
    apply: 'serve',
    enforce: 'post',

    configResolved(resolvedConfig) {
      // store the resolved config
      viteConfig = resolvedConfig;
    },
    configureServer() {
      if (viteConfig.mode !== 'development') return;
      console.log('configureServer');
      setupServer();
    },

    handleHotUpdate({ server }) {
      if (viteConfig.mode !== 'development') return;
      console.log('handleHotUpdate');
      // console.log(viteConfig.server);
      // console.log('server',server.config.server);
      server.ws.send({
        type: 'custom',
        event: 'vite-plugin-htmlcheck:update',
        data: {
          ...config,
        },
      });
      // return [];
    },

    transformIndexHtml(html) {
      if (viteConfig.mode !== 'development') return html;

      // exit js for client already injected
      if (html.includes('<!-- vite-plugin-htmlcheck -->')) return html;

      return `${html}
      ${injectScriptToClient({ config })}`;
    },

    transform(source, id) {
      if (viteConfig.mode !== 'development') return;

      if (
        !source.includes('</html>')
        || source.includes('<!-- vite-plugin-htmlcheck -->')
      ) {
        // exit js for client already injected
        return {
          code: source,
          // eslint-disable-next-line unicorn/no-null
          map: null,
        };
      }

      console.log('transform', id);

      const code = source.replace(
        '</html>',
        `</html>${injectScriptToClient({ config })}`,
      );

      // isValidationInjected = true;

      return {
        code,
        // eslint-disable-next-line unicorn/no-null
        map: null,
      };
    },
  };
};

export default htmlCheck;

export {
  htmlCheck,
};
