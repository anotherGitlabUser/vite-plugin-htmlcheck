import fs from 'node:fs'
import path from 'node:path'
import {fileURLToPath} from 'url';

import {
  mergeConfig,
} from './utils/config.js'; // eslint-disable-line import/extensions
const __filename = fileURLToPath(import.meta.url);


const __dirname = path.dirname(__filename);
const resolve = (p) => path.resolve(__dirname, p)
const yolo = (userConfig = {}) => {
  const config = mergeConfig({ userConfig });

  return {
    name: 'vite-plugin-test',
    apply: 'serve',
    enforce: 'post',

    configureServer(server) {
      console.log('confureServer')

        server.middlewares.use((req, res, next) => {
          // custom handle request...
          let [url] = req.originalUrl.split('?')
          console.log('url', req.url);

          next()
        })

    }
  };
};

export {
  // eslint-disable-next-line import/prefer-default-export
  yolo,
};
