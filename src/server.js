import puppeteer from 'puppeteer';
import url from 'node:url';
import http from 'node:http';

import { HtmlValidate } from 'html-validate';
import { $fetch } from 'ohmyfetch';
import { AxePuppeteer } from '@axe-core/puppeteer';

import {
  store,
} from './utils/config.js';

import {
  md5FromString,
} from './utils/helpers.js';

const processedHtml = new Map();

const responseHeaders = {
  'Access-Control-Allow-Methods': 'GET',
  'Access-Control-Allow-Origin': '*',
  'Content-Type': 'application/json',
};

const htmlValidateInstance = () => {
  const { ...htmlValidateConfig } = store.config.htmlValidate;

  return new HtmlValidate(htmlValidateConfig);
};

const validateHtml = async (htmlString) => {
  const hrstart = process.hrtime();

  const report = htmlValidateInstance().validateString(htmlString);
  const hrend = process.hrtime(hrstart);
  console.info('validateHtml Execution time (hr): %ds %dms', hrend[0], hrend[1] / 1_000_000);

  return ['htmlValidate', { ...report }];
};

const validateAxe = async (query) => {
  const hrstart = process.hrtime();
  const browser = await puppeteer.launch();
  const page = await browser.newPage();
  await page.setBypassCSP(true);

  await page.goto(query);

  // ref: https://github.com/dequelabs/axe-core/blob/master/doc/API.md#api-name-axeconfigure
  const { ...axeValidationConfig } = store.config.axe;

  const report = await new AxePuppeteer(page)
    .options(axeValidationConfig)
    .analyze();

  await page.close();
  await browser.close();
  const hrend = process.hrtime(hrstart);
  console.info('validateAxe Execution time (hr): %ds %dms', hrend[0], hrend[1] / 1_000_000);

  return ['axe', { ...report }];
};

const sendResponseSuccess = ({ response }) => {
  response.writeHead(200, responseHeaders);

  // end the response
  response.end(JSON.stringify(processedHtml.get(url)?.result));
};

/**
 *
 * @param {string} htmlString
 * @param {string} query
 * @returns {Promise<Awaited<any>[]> | undefined}
 */
const validateUrl = async ({ htmlString, query }) => {
  let result;
  const validatorsToExecute = [];

  if (store?.config?.validators.includes('htmlValidate')) validatorsToExecute.push(await validateHtml(htmlString));
  if (store?.config?.validators.includes('axe')) validatorsToExecute.push(await validateAxe(query));

  try {
    result = await Promise.all(validatorsToExecute);
  } catch {
    return result;
  }

  return result;
};

const server = http.createServer(async (request, response) => {
  const { pathname } = url.parse(request.url, false);
  const { query: { query = '' } = {} } = url.parse(request.url, true);

  if (!(
    pathname === '/api'
    && request.method === 'GET'
  )) {
    response.writeHead(404, responseHeaders);
    return response.end(JSON.stringify({ message: 'Route not found' }));
  }

  let htmlString = '';

  try {
    htmlString = await $fetch(query);
  } catch {
    response.writeHead(500, responseHeaders);
    return response.end(JSON.stringify({ message: `Something went wrong fetching ${query}` }));
  }

  const currentMd5 = md5FromString(htmlString);

  if (processedHtml.get(url)?.md5 === currentMd5) return sendResponseSuccess({ response });

  const result = await validateUrl({ htmlString, query });

  if (!Array.isArray(result)) {
    response.writeHead(500, responseHeaders);
    return response.end(JSON.stringify({ message: `Something went wrong validating ${query}` }));
  }

  processedHtml.set(url, { result, md5: currentMd5 });
  return sendResponseSuccess({ response });
});

const setupServer = () => server.listen(store.config.serverPort, () => {
  console.log(`server started on port: ${store.config.serverPort}`);
});

export {
  // eslint-disable-next-line import/prefer-default-export
  setupServer,
};
