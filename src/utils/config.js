/**
 *
 * @type {Set<string>}
 */
const axeResultsToShow = new Set([
  'id',
  'impact',
  'description',
  'help',
]);

/**
 *
 * @type {Set<string>}
 */
const htmlValidateResultsToShow = new Set([
  'ruleId',
  'message',
  'ruleUrl',
]);

/**
 *
 * @type {{waitAfterLoad: number, validators: string[], clearOnViolation: boolean, serverPort: number}}
 */
const defaultConfig = {
  clearOnViolation: true,
  serverPort: 5000,
  waitAfterLoad: 800,
  validators: [
    'axe',
    'htmlValidate',
  ],
  htmlValidate: {
    extends: ['html-validate:recommended'],
  },
  axe: {},
};

const mapSeverity = new Map([
  [0, ''],
  [1, 'moderate'],
  [2, 'critical'],
]);

/**
 *
 * @type {{minor: string, critical: string, serious: string, moderate: string}}
 */
const consoleIncidentStyles = {
  minor: 'background-color: rgba(244, 244, 157, 0.6); padding: 0 5px;',
  moderate: 'background-color: rgba(244, 244, 157, 0.6); padding: 0 5px;',
  serious: 'background-color: rgba(247, 172, 134, 0.6); padding:0 5px;',
  critical: 'background-color: rgba(247, 172, 134, 0.6); padding: 0 5px;',
};

/**
 *
 * @param {Object} userConfig
 * @returns {{waitAfterLoad: number, clearOnViolation: boolean}}
 */
const mergeConfig = ({ userConfig = {} }) => {
  const a = {
    ...defaultConfig,
    ...userConfig,
  };

  return a;
};

const store = new Proxy(
  {
    config: {},
  },
  {
    get(target, property) {
      return target[property];
    },
    set(target, property, value) {
      // eslint-disable-next-line no-param-reassign
      target[property] = value;

      return true;
    },
  },
);

const injectScriptToClient = ({ config }) => `<!-- vite-plugin-htmlcheck -->
      <script type="module">
         import {
         initAxeRun,
         } from '/node_modules/vite-plugin-htmlcheck/src/vitePluginHtmlCheck.js';

         initAxeRun(${JSON.stringify(config)})
      </script>
      <!-- /vite-plugin-htmlcheck -->`;

export {
  axeResultsToShow,
  defaultConfig,
  htmlValidateResultsToShow,
  consoleIncidentStyles,
  store,
  injectScriptToClient,
  mapSeverity,
  mergeConfig,
};
