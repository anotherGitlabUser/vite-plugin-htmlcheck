import crypto from 'node:crypto';


/**
 *
 * @param {string} data
 * @returns {string}
 */
const md5FromString = (data) => crypto.createHash('md5').update(data).digest('hex');

export {
  md5FromString,
};
