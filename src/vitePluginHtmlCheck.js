// eslint-disable-next-line import/extensions,import/no-absolute-path,import/no-unresolved
import { createHotContext } from '/@vite/client';

import {
  axeResultsToShow,
  consoleIncidentStyles,
  htmlValidateResultsToShow,
  mapSeverity,
  mergeConfig,
  store,
} from './utils/config.js';


/**
 *
 * @param {function} fn
 * @param {number} ms
 * @returns {(function(...[*]): void)|*}
 */
const debounce = (function_, ms = 0) => {
  let timeoutId;
  return (...arguments_) => {
    clearTimeout(timeoutId);
    timeoutId = setTimeout(() => function_.apply(this, arguments_), ms);
  };
};

const htmlValidatorResultsToConsole = ({
  validationResults,
}) => {
  const results = validationResults?.htmlValidate?.results || [];

  // eslint-disable-next-line no-restricted-syntax
  for (const result of results) {
    // eslint-disable-next-line no-restricted-syntax
    for (const message of result.messages) {
      console.groupCollapsed(
        `%chtml-validator ${mapSeverity.get(message.severity)} violation detected`,
        consoleIncidentStyles[mapSeverity.get(message.severity)],
        message.ruleId,
      );

      // eslint-disable-next-line no-restricted-syntax,no-console
      for (const key of htmlValidateResultsToShow) console.info(`${key}: ${message[key]}`);

      if (message.selector && message.selector?.length > 0) {
        // eslint-disable-next-line no-restricted-syntax,no-console
        console.info('element:', document.querySelector(message.selector));
      } else {
        console.info(`check Network Response on Line ${message.line} colum ${message.column}`);
      }
      console.groupEnd(message.ruleId);
    }
  }
};

const axeResultsToConsole = ({
  validationResults,
}) => {
  const violations = validationResults?.axe?.violations || [];

  // eslint-disable-next-line no-restricted-syntax
  for (const violation of violations) {
    console.groupCollapsed(
      `%caxe ${violation.impact} violation detected`,
      consoleIncidentStyles[violation.impact],
      violation.id,
    );

    // eslint-disable-next-line no-restricted-syntax,no-console
    for (const key of axeResultsToShow) console.info(`${key}: ${violation[key]}`);

    const affectedNodes = violation.nodes.map((node) => node.target);
    // eslint-disable-next-line no-restricted-syntax,no-console
    for (const affectedNode of affectedNodes) console.info('element:', document.querySelector(affectedNode));

    console.groupEnd(violation.id);
  }
};

const axeRun = async () => {
  let apiResult;

  const {
    hostname,
    protocol,
  } = new URL(window.location);

  const requestQuery = new URLSearchParams({
    query: window.location,
    validators: store.config.validators,
  }).toString();



  try {
    const response = await fetch(`${protocol}//${hostname}:${store.config.serverPort}/api?${requestQuery}`);
    apiResult = await response.json();
  } catch {
    return;
  }

  const validationResults = Object.fromEntries(apiResult);

  const {
    axe: {
      violations = [],
    } = {},
    htmlValidate: {
      valid = true,
    } = {},
  } = validationResults;

  console.log('store.config.clearOnViolation', store.config);
  if (
    store.config.clearOnViolation
    && (
      violations?.length > 0
      || !valid
    )
  ) console.clear();

  if (violations?.length) axeResultsToConsole({ config: store.config, validationResults });
  if (!valid) htmlValidatorResultsToConsole({ config: store.config, validationResults });
};

const initAxeRun = (config) => {

  import.meta.hot = createHotContext('/@fs/vite-plugin-htmlcheck/src/vitePluginHtmlCheck.js');

  const mergedConfig = mergeConfig({ userConfig: config });

  store.config = mergedConfig;

  console.log('import.meta.hot', import.meta);
  const debouncedAxe = debounce(() => axeRun(mergedConfig), mergedConfig.waitAfterLoad);

  debouncedAxe();

  if (import.meta.hot) {
    import.meta.hot.on('vite-plugin-htmlcheck:update', () => {
      console.log('update');
      console.log('config.waitAfterLoad', mergedConfig.waitAfterLoad);
      debouncedAxe();
    });
  }
};


export {
  initAxeRun,
  axeRun,
};
